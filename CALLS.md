## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Keygen. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Keygen.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Keygen. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountUserId(id, account, callback)</td>
    <td style="padding:15px">Retrieve a user</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountUser(account, bodyQuery, callback)</td>
    <td style="padding:15px">Adds a user</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountUserWithOptions(account, queryData, callback)</td>
    <td style="padding:15px">List all users</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountUsersId(account, id, bodyQuery, callback)</td>
    <td style="padding:15px">Update a user</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountUsersId(account, id, callback)</td>
    <td style="padding:15px">Deletes a user</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountUsersIdActionsUpdatePassword(account, id, bodyQuery, callback)</td>
    <td style="padding:15px">Update a user's password</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users/{pathv2}/actions/update-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountUsersIdActionsResetPassword(account, id, bodyQuery, callback)</td>
    <td style="padding:15px">Reset a user's password</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/users/{pathv2}/actions/reset-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicenses(account, bodyQuery, callback)</td>
    <td style="padding:15px">Creates a new license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountLicenses(account, limit, suspended, expired, unassigned, product, policy, user, machine, callback)</td>
    <td style="padding:15px">List all license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountLicensesId(id, account, callback)</td>
    <td style="padding:15px">Retrieve a license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAccountsAccountLicensesId(id, account, bodyQuery, callback)</td>
    <td style="padding:15px">Update a license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountLicensesId(id, account, callback)</td>
    <td style="padding:15px">Delete a license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountLicensesIdActionsValidate(id, account, callback)</td>
    <td style="padding:15px">Quick validate license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsValidate(id, account, meta, callback)</td>
    <td style="padding:15px">Validate license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesActionsValidateKey(account, meta, callback)</td>
    <td style="padding:15px">Validate Key</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/actions/validate-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsSuspend(id, account, callback)</td>
    <td style="padding:15px">Suspend license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsReinstate(id, account, callback)</td>
    <td style="padding:15px">Reinstate license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/reinstate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsRenew(id, account, callback)</td>
    <td style="padding:15px">Renew license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsRevoke(id, account, callback)</td>
    <td style="padding:15px">Revoke license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsCheckIn(id, account, callback)</td>
    <td style="padding:15px">Check-In license</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsIncrementUsage(id, account, meta, callback)</td>
    <td style="padding:15px">Increment usage</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/increment-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsDecrementUsage(id, account, meta, callback)</td>
    <td style="padding:15px">Decrement usage</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/decrement-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdActionsResetUsage(id, account, callback)</td>
    <td style="padding:15px">Reset usage</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/actions/reset-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountLicensesIdTokens(id, account, data, callback)</td>
    <td style="padding:15px">Generate an active token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountsAccountLicensesIdPolicy(id, account, data, callback)</td>
    <td style="padding:15px">Change policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountsAccountLicensesIdUser(id, account, data, callback)</td>
    <td style="padding:15px">Change user</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/licenses/{pathv2}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountPolicies(account, data, callback)</td>
    <td style="padding:15px">Create a policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountPolicies(account, limit, product, callback)</td>
    <td style="padding:15px">List all policies</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountPoliciesId(id, account, callback)</td>
    <td style="padding:15px">Retrieve a policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAccountsAccountPoliciesId(id, account, data, callback)</td>
    <td style="padding:15px">Update a policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountPoliciesId(id, account, callback)</td>
    <td style="padding:15px">Delete a policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountPoliciesIdPool(id, account, callback)</td>
    <td style="padding:15px">Retrieve a policy</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/policies/{pathv2}/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountProducts(account, data, callback)</td>
    <td style="padding:15px">Create a product</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountProducts(account, limit, callback)</td>
    <td style="padding:15px">List all products</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountProductsId(id, account, data, callback)</td>
    <td style="padding:15px">Retrieve a product</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAccountsAccountProductsId(id, account, data, callback)</td>
    <td style="padding:15px">update a product</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountProductsId(id, account, callback)</td>
    <td style="padding:15px">Delete a product</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountProductsIdTokens(id, account, callback)</td>
    <td style="padding:15px">Generate a product token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/products/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountTokens(account, data, callback)</td>
    <td style="padding:15px">Generate a token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountTokens(account, limit, callback)</td>
    <td style="padding:15px">List all tokens</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountTokensId(id, account, callback)</td>
    <td style="padding:15px">Retrieve a token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountsAccountTokensId(id, account, callback)</td>
    <td style="padding:15px">Regenerate a token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountsAccountTokensId(id, account, callback)</td>
    <td style="padding:15px">Revoke a token</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountMachines(account, limit, callback)</td>
    <td style="padding:15px">List all tokens</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsAccountMachinesId(account, id, callback)</td>
    <td style="padding:15px">Retrieve a machine</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/machines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountsAccountMachines(account, data, callback)</td>
    <td style="padding:15px">Activate a machine</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/machines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
